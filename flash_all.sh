#!/bin/bash

for i in $(ls | grep -e .py -e .html); do
  ./venv/bin/python ~/.local/share/JetBrains/PyCharmCE2023.2/intellij-micropython/scripts/microupload.py -C ./ -v /dev/ttyUSB0 $i
done
