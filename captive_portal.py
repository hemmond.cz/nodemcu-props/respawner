import gc
import network
import ubinascii as binascii
import uselect as select
import utime as time

from captive_dns import DNSServer
from captive_http import HTTPServer
from configuration import Configuration


class CaptivePortal:
    AP_IP = "192.168.4.1"

    def __init__(self):
        self.local_ip = self.AP_IP
        self.ap_if = network.WLAN(network.AP_IF)

        self.config = Configuration()

        if self.config.ssid is None:
            self.essid = b"ESP8266-%s" % binascii.hexlify(self.ap_if.config("mac")[-3:])
        else:
            self.essid = self.config.ssid

        self.dns_server = None
        self.http_server = None
        self.poller = select.poll()

        self.conn_time_start = None

    def start_access_point(self):
        # sometimes need to turn off AP before it will come up properly
        self.ap_if.active(False)
        while not self.ap_if.active():
            print("Waiting for access point to turn on")
            self.ap_if.active(True)
            time.sleep(1)

        # IP address, netmask, gateway, DNS
        self.ap_if.ifconfig(
            (self.local_ip, "255.255.255.0", self.local_ip, self.local_ip)
        )

        if self.config.pwd is not None:
            self.ap_if.config(essid=self.essid, password=self.config.pwd)
            print("Configuring WiFi with password.")
        else:
            self.ap_if.config(essid=self.essid, authmode=network.AUTH_OPEN)
            print("Configuring WiFi with no authentication.")
        print("AP mode configured:", self.ap_if.ifconfig())

    def handle_dns(self, sock, event, others):
        if sock is self.dns_server.sock:
            # ignore UDP socket hangups
            if event == select.POLLHUP:
                return True
            self.dns_server.handle(sock, event, others)
            return True
        return False

    def handle_http(self, sock, event, others):
        # try:
        self.http_server.handle(sock, event, others)
        # except:
        #     pass

    def cleanup(self):
        print("Cleaning up")
        if self.dns_server:
            self.dns_server.stop(self.poller)
        gc.collect()

    def start(self):
        # turn off station interface to force a reconnect

        print("\n\nStarting captive portal")
        self.start_access_point()

        if self.http_server is None:
            self.http_server = HTTPServer(self.poller, self.local_ip, self.config)
            print("Configured HTTP server")
        if self.dns_server is None:
            self.dns_server = DNSServer(self.poller, self.local_ip)
            print("Configured DNS server")

        try:
            while True:
                gc.collect()
                # check for socket events and handle them
                for response in self.poller.ipoll(1000):
                    sock, event, *others = response
                    is_handled = self.handle_dns(sock, event, others)
                    if not is_handled:
                        self.handle_http(sock, event, others)

        except KeyboardInterrupt:
            print("Captive portal stopped")
        self.cleanup()
