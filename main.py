from machine import Pin, Signal

LEDS_COUNT = 8      # Number of NeoPixel LEDS on the device.

# Machine GPIO are numbered by chip GPIO IN/OUTs, not by board numbers

print("\n\n")   # Give debug outputs some distance from boot log (which has different baud rate)

wifi_switch = Signal(Pin(12, Pin.IN, Pin.PULL_UP), invert=True)
factory_reset = Signal(Pin(13, Pin.IN, Pin.PULL_UP), invert=True)


if factory_reset.value() == 1:
    print("FACTORY RESET starting.")

    from configuration import Configuration
    from neopixel import NeoPixel
    from time import sleep
    np = NeoPixel(Pin(4), 1)

    np[0] = (128, 0, 0)
    np.write()

    cfg = Configuration()
    cfg.factory_reset()

    print("FACTORY RESET done.")
    while True:
        np[0] = (0, 128, 0)
        np.write()
        sleep(1)
        np[0] = (0, 0, 0)
        np.write()
        sleep(1)

elif wifi_switch.value():
    from captive_portal import CaptivePortal

    portal = CaptivePortal()
    portal.start()

else:
    from running_modes import RunningModes
    from configuration import Configuration

    cfg = Configuration()

    RunningModes(cfg, LEDS_COUNT)


