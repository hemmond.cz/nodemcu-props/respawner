import uos


class Configuration:
    """
    This class manages storage and retrieval of all device configuration data.
    """
    WIFI_FILE = "./wifi.cfg"            # Configure wifi SSID and password.
    CONFIG_FILE = "./mtz_config.cfg"    # Configure device function variables
    STATS_FILE = "./stats.cfg"          # Runtime stats user might want to see

    # Currently supported modes:
    MODES = ("simple", "cyclic", "limited_lives", "random_green")

    def __init__(self, mode=None, password=None):
        self.ssid = None
        self.pwd = None

        self._load_wifi()

        # Konfiguracni hodnoty:
        self.mode = "simple"  # REZIM: Režim (ve kterém režimu zařízení běží)
        self.t_red = 10  # CAS CERVENA: Doba trvání fáze „oživování“ (běhání červeného světýlka)
        self.t_green = 5  # CAS ZELENA: Doba trvání fáze „oživení dokončeno“ (svitu zelených světýlek)
        self.t_black = 5  # CAS TMA: Doba trvání fáze „chlazení“ (vše zhasnuté, nelze spustit další sekvenci)
        self.lives_count = 0  # POCET ZIVOTU: Počet životů po které zařízení přestane oživovat.
        self.show_remaining = False  # ZBYV.ZIVOTY (zda zobrazovat zbyvajici zivoty)

        self._load_cfg()

        # Statistiky:
        self.press_count = 0    # Stisknuto N krát: Kolikrát byla stiskem tlačítka spuštěna fáze oživování.

        self._load_stats()

        print("Config constructor finished:\n", self)

    def factory_reset(self):
        self._remove_wifi()
        self._delete_cfg()
        self._reset_stats()

    def set_wifi(self, ssid, password=None):
        if ssid is not None:
            self.ssid = ssid
            self.pwd = password
            self._write_wifi()
        else:
            self._remove_wifi()

    def _write_wifi(self):
        """Write current WiFi configuration values to WIFI_FILE"""
        with open(self.WIFI_FILE, "wb") as f:
            if self.pwd is not None:
                f.write(b",".join([self.ssid, self.pwd]))
            else:
                f.write(b",".join([self.ssid]))
        print("Wrote wifi config to {:s}".format(self.WIFI_FILE))

    def _load_wifi(self):
        """Load user-defined wifi configuration from file"""
        try:
            with open(self.WIFI_FILE, "rb") as f:
                contents = f.read().split(b",")
            if len(contents) >= 1:
                self.ssid = contents[0]
                print("Loaded WiFi SSID from {:s}".format(self.WIFI_FILE))
            if len(contents) == 2:
                pwd = str(contents[1])
                if len(pwd) >= 8:
                    self.pwd = pwd
                    print("Loaded WiFi PASS from {:s}".format(self.WIFI_FILE))
                else:
                    self.pwd = None
        except OSError:
            pass

    def _remove_wifi(self):
        """Delete wifi credentials."""
        try:
            uos.remove(self.WIFI_FILE)
        except OSError:
            pass

    def set_cfg(self, mode, t_red, t_green, t_black, lives_count, show_remaining):
        if mode in self.MODES:
            self.mode = mode

        self.t_red = max(0, int(t_red))
        self.t_green = max(0, int(t_green))
        self.t_black = max(0, int(t_black))
        self.lives_count = max(0, int(lives_count))
        self.show_remaining = bool(show_remaining)

        self._write_cfg()

    def _write_cfg(self):
        """Write current configuration values to CONFIG_FILE"""
        data = ",".join([self.mode, str(self.t_red), str(self.t_green), str(self.t_black), str(self.lives_count),
                         str(int(self.show_remaining))])
        with open(self.CONFIG_FILE, "wb") as f:
            f.write(data.encode())
        print("Wrote configuration to {:s}".format(self.CONFIG_FILE))

    def _load_cfg(self):
        """Load user-modified values from CONFIG_FILE"""
        try:
            with open(self.CONFIG_FILE, "rb") as f:
                contents = f.read().split(b",")
            if len(contents) == 6:
                self.mode = str(contents[0].decode())
                self.t_red = int(contents[1])
                self.t_green = int(contents[2])
                self.t_black = int(contents[3])
                self.lives_count = int(contents[4])
                self.show_remaining = bool(int(contents[5]))
                print("Loaded configuration from {:s}".format(self.CONFIG_FILE))
        except OSError:
            pass

    def _delete_cfg(self):
        try:
            uos.remove(self.CONFIG_FILE)
        except OSError:
            pass

    def increment_button_press_counter(self):
        self.press_count = self.press_count+1
        self._write_stats()

    def _load_stats(self):
        try:
            with open(self.STATS_FILE, "rb") as f:
                contents = f.read().split(b",")
            if len(contents) == 1:
                self.press_count = int(contents[0])
                print("Loaded stats from {:s}".format(self.STATS_FILE), "press count:", self.press_count)
        except OSError:
            pass

    def _write_stats(self):
        data = ",".join([str(self.press_count)])
        with open(self.STATS_FILE, "wb") as f:
            f.write(data.encode())
        print("Wrote stats to {:s}".format(self.STATS_FILE))

    def _reset_stats(self):
        self.press_count = 0
        try:
            uos.remove(self.STATS_FILE)
        except OSError:
            pass

    def __repr__(self):
        return f"CFG:\n\t{self.mode}, t_rgb: {self.t_red}/{self.t_green}/{self.t_black}, l: {self.lives_count}, show: {self.show_remaining}\n\tPresses: {self.press_count}"
