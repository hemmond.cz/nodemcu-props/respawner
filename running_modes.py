from neopixel import NeoPixel
from machine import Pin, Signal
from time import sleep, ticks_ms, ticks_diff
from math import ceil
from random import getrandbits

BRIGHTNESS = 2


def randrange(start, stop):
    def bit_length(n):
        bits = 0
        while n:
            bits += 1
            n >>= 1
        return bits

    start, stop = min(start, stop), max(start, stop)
    num_choices = stop - start
    num_bits = bit_length(num_choices)

    while True:
        value = getrandbits(num_bits)
        if value < num_choices:
            return start + value


class RunningModes:
    DO_FLASH = False

    DARK = (0, 0, 0)
    # RED = (255, 0, 0)
    # GREEN = (0, 255, 0)
    RED = (BRIGHTNESS, 0, 0)
    GREEN = (0, BRIGHTNESS, 0)
    BLUE = (0, 0, BRIGHTNESS)

    def __init__(self, cfg, leds_count):
        self.config = cfg
        self.leds_count = leds_count

        self.np = NeoPixel(Pin(4), leds_count)
        self.flash_pin = Pin(2, Pin.OUT)

        self.button = Signal(Pin(14, Pin.IN, Pin.PULL_UP), invert=True)     # 1 = button pressed.

        self.current_red = None

        self.flash_pin.value(1)  # power down onboard LED (it is inverted - 1=led is off).
        self._all_leds(self.DARK)

        self._select_op_mode()

    def _select_op_mode(self):
        # self.debug_running_light()
        if self.config.mode == "simple":
            self.mode_simple()
        elif self.config.mode == "cyclic":
            self.mode_cyclic()
        elif self.config.mode == "limited_lives":
            self.mode_limited_lives()
        elif self.config.mode == "random_green":
            self.mode_random_green()

    def _button_pressed(self, blocking=True):
        """Blocking function, returns True when button press is detected, implements also Debouncing"""
        while True:
            if self.button.value() == 1:
                sleep(0.05)
                if self.button.value() == 1:
                    return True
            if not blocking:
                return False
            else:
                sleep(0.01)

    def _next_red_led(self):
        """Moves currently lit LED to next one."""
        for led in range(0, self.leds_count):   # turn all leds dark,
            self.np[led] = self.DARK

        if self.current_red is None:    # sanitize first run
            self._clear_last_led()
            self.current_red = self.leds_count-1

        # increment counter and light up required led
        self.current_red = (self.current_red+1) % self.leds_count
        self.np[self.current_red] = self.RED
        self.np.write()

    def _blink_leds_error(self, blinks_count=3):
        for i in range(0, blinks_count):
            self._all_leds(self.RED)
            sleep(0.12)
            self._all_leds(self.DARK)
            sleep(0.12)

    def _all_leds(self, color, percentage=100):
        """Lights specified percent of LEDS to specified color.
        If 0 percent is specified, every second LED will be lit"""
        if self.current_red is not None:
            self._clear_last_led()

        single_led_percentage = 1/self.leds_count*100
        for led in range(0, self.leds_count):   # turn all leds to specified color
            if percentage > 0:
                if led*single_led_percentage <= percentage:
                    self.np[led] = color
                else:
                    self.np[led] = self.DARK
            else:
                if led % 2 == 0:
                    self.np[led] = color
                else:
                    self.np[led] = self.DARK
        self.np.write()

    def _running_red_stage(self, duration):
        trigger_time = ticks_ms()
        print("RUNNING_RED started on", trigger_time, "ms. Will run for:", duration, "s.")

        while ticks_diff(ticks_ms(), trigger_time) < duration * 1000:
            self._next_red_led()
            sleep(1 / self.leds_count)

    def _clear_last_led(self):
        """Call this when changing stage"""
        self.current_red = None

    def debug_running_light(self):
        """Mode for testing of NeoPixel leds. Not an interactive mode. """
        flash = False

        while True:
            for i in range(0, self.leds_count):
                print("Processing ", i)
                self.np[(0 + i) % self.leds_count] = (0, 0, 0)
                self.np[(1 + i) % self.leds_count] = (0, 128, 0)
                self.np[(2 + i) % self.leds_count] = (255, 0, 0)
                self.np.write()

                if self.DO_FLASH:
                    self.flash_pin.value(int(flash))
                else:
                    self.flash_pin.value(1)     # onboard LED is inverted.
                flash = not flash
                sleep(0.05)

    def mode_simple(self):
        # Simple mode
        print("Simple mode started")
        while True:
            while not self._button_pressed():
                # Waiting for trigger in loop.
                sleep(0.01)

            # Button pressed
            self.config.increment_button_press_counter()
            self._running_red_stage(self.config.t_red)

            print("RED stage finished on time:", ticks_ms())
            self._all_leds(self.GREEN)
            sleep(self.config.t_green)
            self._all_leds(self.DARK)

            trigger_time = ticks_ms()
            print("GREEN stage finished on time:", trigger_time)

            while ticks_diff(ticks_ms(), trigger_time) < self.config.t_black * 1000:
                if self._button_pressed(blocking=False):
                    self._blink_leds_error()
                sleep(0.01)

    def mode_cyclic(self):
        # Cyclic mode
        print("Cyclic mode started")
        while not self._button_pressed():
            # Waiting for first trigger.
            sleep(0.01)

        while True:
            self._running_red_stage(self.config.t_red)

            self._all_leds(self.GREEN)
            sleep(self.config.t_green)
            self._all_leds(self.DARK)
            sleep(self.config.t_black)

    def mode_limited_lives(self):
        # Limited lives mode
        print("Limited lives mode started")
        while True:
            while not self._button_pressed():
                # Waiting for trigger in loop.
                sleep(0.01)

            # Button pressed
            remaining_lives = self.config.lives_count-self.config.press_count
            print("Remaining lives:", remaining_lives-1, "/", self.config.lives_count,
                  "(", (remaining_lives-1)/self.config.lives_count*100, "%)")

            if remaining_lives > 0:
                self.config.increment_button_press_counter()
                self._running_red_stage(self.config.t_red)

                print("RED stage finished on time:", ticks_ms())
                percentage = 100
                if self.config.show_remaining:
                    percentage = ceil((remaining_lives-1)/self.config.lives_count*100)

                self._all_leds(self.GREEN, percentage)
                sleep(self.config.t_green)
                self._all_leds(self.DARK)

                trigger_time = ticks_ms()
                print("GREEN stage finished on time:", trigger_time)

                while ticks_diff(ticks_ms(), trigger_time) < self.config.t_black * 1000:
                    if self._button_pressed(blocking=False):
                        self._blink_leds_error()
                    sleep(0.01)

            else:
                # Ran out of lives:
                self._blink_leds_error(5)

    def mode_random_green(self):
        print("Random green mode started")

        while True:
            stage_time = randrange(self.config.t_red, self.config.t_black)
            print(f"Red stage: <{self.config.t_red}, {self.config.t_black}>, selected number: {stage_time}")
            self._running_red_stage(stage_time)

            trigger_time = ticks_ms()
            self._all_leds(self.GREEN)
            while ticks_diff(ticks_ms(), trigger_time) < self.config.t_green * 1000:
                if self._button_pressed(blocking=False):
                    self.config.increment_button_press_counter()
                    self._all_leds(self.BLUE)
                    sleep(0.5)
                    break
            self._all_leds(self.DARK)
