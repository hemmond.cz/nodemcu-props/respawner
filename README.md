# Respawner V3
This readme is for now just a notepad for me. Proper readme will be added later on, this file is a placeholder. 

GPIO are numbered by chip GPIO IN/OUTs, not by board numbers

# Hemmond's notes
To modify this boilerplate to run something, these files must be modified: 
configuration.py:
    Here are all configuration constants for the entire device, including optional wifi ssid/password. 
    Variables Configuration.ssid and Configuration.password are used by captive_portal.py when setting up Access Point. 
captive_http.py:
    This script serves HTML files for user to configure the device and processes forms sent by user. 

captive_dns.py:
captive_portal.py:
server.py:
    Backbone libraries working as-is, no change required/expected. 
    Captive_portal.py requires Configuration.ssid and Configuration.password variables to exist (both can be None).

boot.py:
    No modifications expected
main.py:
    Modify this as required. Instantiate captive_portal from here when necessary (detect switch to config mode here).

# Wishlist and TODO:
- Add user-configurable value of brightness control (range 5-255): how dim/lit should the device be, less brightness = more battery power.
- Add statistics retrieval and reset option in configuration and TEST it.

# Install micrpython to new NodeMCU:
pip install esptool
esptool.py --port /dev/ttyUSB0 erase_flash
esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_mode=dio --flash_size=4MB 0 ESP8266_GENERIC-20231005-v1.21.0.bin

# Code I've been building on
CSS tricks thanks to:
https://nojs.amyskapers.dev/#faqs
Original captive portal code:
https://github.com/anson-vandoren/esp8266-captive-portal/blob/master/LICENSE
